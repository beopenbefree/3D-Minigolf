extends RigidBody

signal stopped

func _ready():
	angular_velocity = Vector3()

func shoot(angle, power):
	var force = Vector3(0, 0, -1).rotated(Vector3(0, 1, 0), angle)
	apply_impulse(Vector3(), force * power / 5)

func _integrate_forces(state):
	if state.linear_velocity.length() < 0.1:
		state.linear_velocity = Vector3()
		emit_signal("stopped")
