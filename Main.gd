extends Node

var shots = 0
var state
var power = 0
var power_change = 1
var power_speed = 100
var angle_change = 1
var angle_speed = 1.1
enum {SET_ANGLE, SET_POWER, SHOOT, WIN}
var hole_dir = 0
var mouse_start_pos
var gimbal_rotate_damp = 2
var mouse_move

func _ready():
	$Arrow.hide()
	$Ball.transform.origin = $Tee.transform.origin
	var screensize = get_viewport().get_visible_rect().size
	mouse_start_pos = Vector2(screensize.x / 2, screensize.y / 3)
	change_state(SET_ANGLE)
	set_start_angle(0)
	$GimbalOut/GimbalIn.rotation.x = -sqrt(2) / 2

func change_state(new_state):
	state = new_state
	mouse_move = 0
	match state:
		SET_ANGLE:
			$Arrow.transform.origin = $Ball.transform.origin
			$Arrow.show()
			set_start_angle()
			get_viewport().warp_mouse(mouse_start_pos)
		SET_POWER:
			pass
		SHOOT:
			$Arrow.hide()
			$Ball.shoot($Arrow.rotation.y, power)
			shots += 1
			$UI.update_shots(shots)
		WIN:
			$Ball.hide()
			$Arrow.hide()

func _input(event):
	if event.is_action_pressed("click"):
		match state:
			SET_ANGLE:
				change_state(SET_POWER)
			SET_POWER:
				change_state(SHOOT)
	if event is InputEventMouseMotion:
		if state == SET_ANGLE:
			if mouse_move > 1:
				$Arrow.rotation.y -= event.relative.x / 150
			else:
				mouse_move += 1
		

func _process(delta):
	$GimbalOut.translation = $Ball.transform.origin
	match state:
		SET_ANGLE:
			#animate_angle(delta)
			var delta_angle = $GimbalOut.rotation.y - $Arrow.rotation.y
			if delta_angle >= 0:
				delta_angle = max(delta_angle - floor(delta_angle / (2 * PI)) * 2 * PI, 0)
				if delta_angle <= PI:
					$GimbalOut.rotation.y -= delta_angle * gimbal_rotate_damp * delta
				else:
					$GimbalOut.rotation.y += delta_angle * gimbal_rotate_damp * delta
			else:
				delta_angle = min(0, delta_angle - (floor(delta_angle / (2 * PI)) + 1) * 2 * PI)
				if delta_angle >= -PI:
					$GimbalOut.rotation.y -= delta_angle * gimbal_rotate_damp * delta
				else:
					$GimbalOut.rotation.y += delta_angle * gimbal_rotate_damp * delta
		SET_POWER:
			animate_power_bar(delta)
		SHOOT:
			pass

func animate_power_bar(delta):
	power += power_speed * power_change * delta
	if power >= 100:
		power_change = -1
	if power <= 0:
		power_change = 1
	$UI.update_powerbar(power)
	

func animate_angle(delta):
	$Arrow.rotation.y += angle_speed * angle_change * delta
	if $Arrow.rotation.y > hole_dir + PI / 2:
		angle_change = -1
	if $Arrow.rotation.y < hole_dir - PI / 2:
		angle_change = 1

func set_start_angle(forced_angle=null):
	if forced_angle == null:
		var hole_pos = Vector2($Hole.transform.origin.x, -$Hole.transform.origin.z)
		var ball_pos = Vector2($Ball.transform.origin.x, -$Ball.transform.origin.z)
		hole_dir = (hole_pos - ball_pos).angle()
		$Arrow.rotation.y = hole_dir - PI / 2
	else:
		$Arrow.rotation.y = forced_angle

func _on_Hole_body_entered(body):
	print("Win!")
	change_state(WIN)


func _on_CameraArea_body_entered(body, cameraNum):
	get_node("Camera" + cameraNum).current = true


func _on_Ball_stopped():
	if state == SHOOT:
		change_state(SET_ANGLE)
